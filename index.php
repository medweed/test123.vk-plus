<?php
session_start();
//подключаем класс SaveTest и доступ к бд
include("includes/bd.php");
include("includes/LogRegHelper.php");
//если есть сессия с токеном и сериеей обновляем куки
if (isset($_SESSION['token']) && isset($_SESSION['series']) && isset($_SESSION['id'])) {
    setcookie("token", $_SESSION['token'], time() + 3600 * 24 * 7);
    setcookie("series", $_SESSION['series'], time() + 86400 * 30);
    setcookie("id",$_SESSION['id'],time() + 3600 * 24 * 7);
} //если нет, то используя куки проверяем пользователя
else {
    if (isset($_COOKIE['token']) && isset($_COOKIE['series']) && isset($_COOKIE['id'])) {
        $obj = new  LogRegHelper($DBH);
        $_SESSION['id']=$_COOKIE['id'];
        if ($obj->checkTokenAndSeries($_COOKIE['token'], $_COOKIE['series']) == "err") {
            $err = "В ваш аккаунт был выполнен вход, с использование ваших куков. Войдите еще раз";
        }
    }
}
if(!isset($_SESSION['status'])){
    $obj = new  LogRegHelper($DBH);
    $_SESSION['status'] = $obj->checkStatus($_SESSION['id']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <meta charset="UTF-8">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <title>vk-plus</title>
    <script>
        document.createElement("section");
        document.createElement("header");
        document.createElement("nav");
        document.createElement("article");
        document.createElement("aside");
        document.createElement("footer");
    </script>
</head>
<body>
<div class="side_bar">
    <?php
    //mail("nikolaevandrey18@gmail.com", "My Subject", "Line 1\nLine 2\nLine 3");
    include("src/header.php");
    //если есть сессия, то вход выполнен и логин форму не отображаем, а отображаем менюбар
    if (isset($_SESSION['token']) && isset($_SESSION['series']) && ($_SESSION['status'] == 1)) {
        include("src/navmenu.php");
    } else {
        if(isset($_SESSION['status']) && $_SESSION['status'] == 0){
            print "Вы не подтвердили регистрацию";
        }
        include("login.php");
        if (isset($err)) {
            print $err;
        }
    }

    ?>
</div>
<div class="main">

    <!--content-->
    <?php
    include("src/content.php");
    include("src/footer.php");
    ?>
</div>


</body>
</html>