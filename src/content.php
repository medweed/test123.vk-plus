<div class="header"><h2>Целевая аудитория для Ваших проектов автоматический раскрутки ВКонтакте</h2></div>

<div class="content"><h1 style="text-align: center">Добро пожаловать на сервис раскрутки vk-plus.com</h1><BR>
    <div style="margin:15px; text-align: center">
        Мы занимаемся привлечением заинтересованной живой аудитории в Ваши проекты в социальной сети ВКонтакте.<br><br>

        ВКонтакте — не только самая популярная, но и самая быстрорастущая социальная сеть в СНГ. На его долю
        приходится половина всего русскоязычного интернет-трафика.<br><br>

        Именно поэтому данная социальная сеть является весьма актуальной средой для налаживания связей между
        бизнесом и клиентами. Собственное представительство ВКонтакте в виде группы позволяет объединять целевую
        аудиторию, общаться с потенциальными клиентами, создавать положительный образ бренда, повышать узнаваемость
        компании и товаров, продавать товары и услуги.

        <BR><BR>
        Мы дорожим своей репутацией и можем гарантировать ответственный подход к работе, скорость,
        качество выполнения заказов. Наш сервис работает в автоматическом режиме.<BR><BR>
    </div>

    <h1>Цены</h1>
    <div class="cost"><b>Живые вступившие по стране</b><br>
        <div class="cost_listing">
            <b><span class="digit">100</span></b> человек = <b><span class="digit">200</span></b> рублей,
            <span class="lower_letter">минимальный заказ</span><br>
            <b><span class="digit">500</span></b> человек = <b><span class="digit">750</span></b> рублей<br>
            <b><span class="digit">5000</span></b> человек = <b><span class="digit">6750</span></b> рублей<br>
        </div>

        <b>Живые вступившие по полу</b><br>
        <div style="padding-left: 10px; padding-bottom:4px;">
            <b><span class="digit">100</span></b> человек = <b><span class="digit">200</span></b> рублей,
            <span class="lower_letter">минимальный заказ</span><br>
            <b><span class="digit">500</span></b> человек = <b><span class="digit">750</span></b> рублей<br>
            <b><span class="digit">5000</span></b> человек = <b><span class="digit">6750</span></b> рублей<br>
        </div>

        <b>Живые вступившие по возрасту</b><br>
        <div style="padding-left: 10px; padding-bottom:4px;">
            <b><span class="digit">100</span></b> человек = <b><span class="digit">200</span></b> рублей,
            <span class="lower_letter">минимальный заказ</span><br>
            <b><span class="digit">500</span></b> человек = <b><span class="digit">750</span></b> рублей<br>
            <b><span class="digit">5000</span></b> человек = <b><span class="digit">6750</span></b> рублей<br>
        </div>

        <b>Живые вступившие по стране и полу</b><br>
        <div style="padding-left: 10px; padding-bottom:4px;">
            <b><span class="digit">100</span></b> человек = <b><span class="digit">300</span></b> рублей,
            <span class="lower_letter">минимальный заказ</span><br>
            <b><span class="digit">500</span></b> человек = <b><span class="digit">1150</span></b> рублей<br>
            <b><span class="digit">5000</span></b> человек = <b><span class="digit">10150</span></b> рублей<br>
        </div>

        <b>Живые вступившие по стране и возрасту</b><br>
        <div style="padding-left: 10px; padding-bottom:4px;">
            <b><span class="digit">100</span></b> человек = <b><span class="digit">300</span></b> рублей,
            <span class="lower_letter">минимальный заказ</span><br>
            <b><span class="digit">500</span></b> человек = <b><span class="digit">1150</span></b> рублей<br>
            <b><span class="digit">5000</span></b> человек = <b><span class="digit">10150</span></b> рублей<br>
        </div>

        <b>Живые вступившие по стране, полу и возрасту</b><br>
        <div style="padding-left: 10px; padding-bottom:4px;">
            <b><span class="digit">100</span></b> человек = <b><span class="digit">400</span></b> рублей,
            <span class="lower_letter">минимальный заказ</span><br>
            <b><span class="digit">500</span></b> человек = <b><span class="digit">1500</span></b> рублей<br>
            <b><span class="digit">2000</span></b> человек = <b><span class="digit">5700</span></b> рублей<br>
        </div>

        <b>Живые вступившие по городу, полу и возрасту</b><br>
        <div style="padding-left: 10px; padding-bottom:4px;">
            <b><span class="digit">100</span></b> человек = <b><span class="digit">600</span></b> рублей,
            <span class="lower_letter">минимальный заказ</span><br>
            <b><span class="digit">500</span></b> человек = <b><span class="digit">2250</span></b> рублей<br>
            <b><span class="digit">2000</span></b> человек = <b><span class="digit">8700</span></b> рублей<br>
        </div>

        <b>Живые вступившие по сложным критериям</b><br>
        <div style="padding-left: 10px; padding-bottom:4px;">
            <b><span class="digit">100</span></b> человек = <b><span class="digit">1000</span></b> рублей,
            <span class="lower_letter">минимальный заказ</span><br>
            <b><span class="digit">500</span></b> человек = <b><span class="digit">3500</span></b> рублей<br>
            <b><span class="digit">2000</span></b> человек = <b><span class="digit">13000</span></b> рублей<br>
        </div>
    </div>

</div>