<?php
session_start();
if   (empty($_SESSION['token']) or empty($_SESSION['series']))
{
    //если не существует сессии с логином и паролем, значит на этот файл попал невошедший пользователь. Ему тут не место. Выдаем сообщение    об ошибке, останавливаем скрипт
    exit ("Доступ на эту    страницу разрешен только зарегистрированным пользователям. Если вы    зарегистрированы, то войдите на сайт под своим логином и паролем<br><a    href='index.php'>Главная    страница</a>");
}

session_destroy();
setcookie("token", "", time()-3600);
setcookie("series", "", time()-3600);
exit("<html><head><meta    http-equiv='Refresh' content='0;    URL=http://www.test123.vk-plus.com'></head></html>");
// отправляем пользователя на главную страницу.
