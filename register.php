<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <meta charset="UTF-8">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <title>Register</title>
    <script>
        document.createElement("section");
        document.createElement("header");
        document.createElement("nav");
        document.createElement("article");
        document.createElement("aside");
        document.createElement("footer");
    </script>
</head>
<body>
<div class="side_bar">
    <?php
    include("src/header.php");
    //@TODO: реализовать рекапчу
    ?>

    <div class="login">
        <h2>Регистрация</h2>
        <form action="javascript:void(0);" name="myform" id="myform">
            Ваше имя:<br>
            <input type="text" name="login" value=""><br><br>
            Ваш пароль:<br>
            <input type="password" name="password" value=""><br><br>
            Ваш Email:<br>
            <input type="text" name="email" value=""><br><br>
            <!--Введите код с картинки:<br>
            <p><img src="code/my_codegen.php"></p>
            <p><input type="text" name="code" value=""></p>
            <div class="g-recaptcha" data-sitekey="6LdlfiYTAAAAAFPQsVIuYbSuLb3JJyyfwz5uMQO1"
                 style="transform: scale(0.77);-webkit-transform: scale(0.77);
             transform-origin: 0 0;-webkit-transform-origin:0 0; padding-left: 16px"></div>-->
            <input type="submit" value="Зарегистрироваться" id="submit" onclick="sendform();">
            <br><a href="index.php">Войти</a>
        </form>
    </div>
    <div id="error" style="text-align: center"></div>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script language="JavaScript" type="text/javascript">
        /* запрос на обработку формы, если
         есть ошибки выводятся без обновления страницы
         под формой
         */
        /* адрес сайта */
        var Site = {serverName: 'www.test123.vk-plus.com/'}
        function sendform() {
            var msg = $('#myform').serialize();
            /* блокируем кнопку отправить */
            document.myform.submit.disabled = true;
            /* меняем надпись на кнопке */
            document.myform.submit.value = "Подождите...";
            $.ajax({
                type: 'POST',
                /* адрес php файла, обрабатывающего форму */
                url: "http://" + Site.serverName + "/includes/SaveUser.php",
                data: msg + "&action=sendform",
                cache: false,
                success: function (data) {
                    $("#error").html(data);
                    document.myform.submit.disabled = false;
                    document.myform.submit.value = "Зарегистрироваться";
                }
            });
        }
    </script>
</div>

<div class="main">
    <!--content-->
    <?php
    include("src/content.php");
    include("src/footer.php");
    ?>
</div>


</body>
</html>
