<div class="login">
    <h2>Вход</h2>
    <!--Форма для выполнения входа -->
    <form action="javascript:void(0);" name="enter" id="enter">
        Ваше имя:<br>
        <input type="text" name="login" value=""><br><br>

        Ваш пароль:<br>
        <input type="password" name="password" value=""><br><br>
        <!--<div class="g-recaptcha" data-sitekey="6LdlfiYTAAAAAFPQsVIuYbSuLb3JJyyfwz5uMQO1"
             style="transform: scale(0.77);-webkit-transform: scale(0.77);
             transform-origin: 0 0;-webkit-transform-origin:0 0; padding-left: 16px"></div>-->
        <input type="submit" value="Войти" id="submit" onclick="sendform();">
        <br><a href="register.php">Зарегистрироваться</a>
    </form>
</div>
<div id="error" style="text-align: center"></div>
<script src="https://www.google.com/recaptcha/api.js"></script>
<script language="JavaScript" type="text/javascript">
    /* запрос на обработку формы, если
     есть ошибки выводятся без обновления страницы
     под формой
     */
    /* адрес сайта */
    var Site = {serverName: 'www.test123.vk-plus.com/'}
    function sendform() {
        var msg = $('#enter').serialize();
        /* блокируем кнопку отправить */
        document.enter.submit.disabled = true;
        /* меняем надпись на кнопке */
        document.enter.submit.value = "Подождите...";
        $.ajax({
            type: 'POST',
            /* адрес php файла, обрабатывающего форму */
            url: "http://" + Site.serverName + "/includes/testreg.php",
            data: msg + "&action=sendform",
            cache: false,
            success: function (data) {
                $("#error").html(data);
                document.enter.submit.disabled = false;
                document.enter.submit.value = "Войти";
            }
        });
    }
</script>
</div>
