<?php
session_start()
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <meta charset="UTF-8">
    <title>Title</title>
    <script>
        document.createElement("section");
        document.createElement("header");
        document.createElement("nav");
        document.createElement("article");
        document.createElement("aside");
        document.createElement("footer");
    </script>
</head>
<body>

<div class="side_bar">

    <?php
    include("src/header.php");
    //если есть сессия, то вход выполнен и логин форму не отображаем, а отображаем менюбар
    if(isset($_SESSION['token']) and isset($_SESSION['series'])){
        include("src/navmenu.php");
    } else {
        include("login.php");
    }
    ?>
</div>
<script type="text/javascript">
    //выпадающий блок с доп.информацией по клику на заголовоке
    function checker(id) {
        var obj = document.getElementById('question' + id);
        if (obj.style.display == 'none') {
            obj.style.display = 'block';
        }
        else {
            obj.style.display = 'none';
        }
    }
</script>

<div class="main">
    <div class="header"><h1>Целевая аудитория для Ваших проектов автоматический раскрутки ВКонтакте</h1></div>
    <h2>Вопрос-Ответ</h2>
    <div class="qback" id="qback1" ">
        <table border=0>
            <tr>
                <td rowspan=2 width="25px" valign=top style="line-height:16px"><b>1)</b></td>
                <td><a onClick="checker('1')"
                       style="font-size: 12px; font-weight: bold; line-height:16px" HREF="#1" name="1">Как мне
                        оформить заказ?</a></td>
            </tr>
            <tr>
                <td>
                    <div style="display: none;" id='question1'
                    ">
                    <div style="text-align: justify"><img src="img/example1.jpg"
                                                          style="margin-left: 3px; float: right; padding-bottom: 5px">Для
                        того чтобы оформить заказ, перейдите на <a href="">страницу заказа</a>. <br><br>На
                        открывшейся странице Вы найдете форму заказа. Заполните ее, пожалуйста, аналогично картинке
                        справа.<br><br><b>Ссылка на объект</b> - укажите ссылку на Ваш объект ВК. Наш сервис
                        работает только с <b>группами и встречами</b>. С публичными страницами, мы не работаем, к
                        сожалению.<br><br><b>Необходимое количество</b> - укажите то количество человек, которое
                        хотите заказать. Для каждого критерия есть свой минимальный лимит, учитывайте его,
                        пожалуйста.<br><br><b>Критерии</b> - здесь необходимо указать критерии людей, которые будут
                        привлечены в группу. Критерии указываются в виде ссылок на страницы поиска ВКонтакте. Для
                        того чтобы получить такую ссылку нужно перейти <a
                            href="http://vk.com/search?c[section]=people" target=>на страницу</a>, выбрать желаемые
                        критерии, скопировать ссылку из строки браузера и вставить в соответствующее поле на
                        странице заказа в нашем сервисе. Если необходимо привлечь людей из других сообществ
                        ВКонтакте, необходимо добавить в полученную ссылку параметр <b>&c[group]=ID ГРУППЫ</b>.
                        Можно указывать несколько критериев - по одному на строчку.<br><br>Нажмите кнопку "Далее" и
                        подтвердите правильность введенных данных на следующем шаге. Заказ будет оформлен и поступит
                        к нам на модерацию<br><br><br><a name="1.1"
                                                         style="text-decoration: none; color: rgb(68, 68, 68); font-weight: bold; font-size: 14px">Важные
                            особенности приема заказов</a><br>Все заказы модерируются на соответствие заказанных
                        критериев тематике группы.<br>
                        <br>
                        Например, если Вам необходимо поставить задание в группу магазина женских вещей по критерию
                        "страна", то такой заказ мы не сможем выполнить. Абсолютному большинству мужчин не будет
                        интересна такая продукция, однако они будут попадать под заказанные критерии.<br>
                        <br>
                        Кроме этого есть определенные ограничения для следующих критериев:<br>
                        <b>Живые вступившие по стране</b> - только некоммерческие группы, только Россия, Украина,
                        Белоруссия и Казахстан<br><br>
                        <b>Живые вступившие по полу</b> - только некоммерческие группы<br><br>
                        <b>Живые вступившие по возрасту</b> - только некоммерческие группы, один критерий
                        возраста<br><br>
                        <b>Живые вступившие по стране и полу</b> - коммерческие группы без привязки к городу,
                        критерий страна ограничен Россией, Украиной, Белоруссией и Казахстаном<br><br>
                        <b>Живые вступившие по стране и возрасту</b> - коммерческие группы без привязки к городу,
                        один критерий возраста<br><br>
                        <b>Живые вступившие по стране, полу и возрасту</b> - коммерческие группы без привязки к
                        городу, критерий страна ограничен Россией, Украиной, Белоруссией и Казахстаном<br>
                        <br>
                        Под коммерческими группами мы понимаем группы, которые: <br>
                        1) занимаются продажами каких-либо товаров и услуг<br>
                        2) продвигают сторонний сайт<br>
                        3) рекламируют конкретный бренд<br>
                        Соответственно, все группы, которые не содержат обозначенных признаков, мы считаем
                        некоммерческими.
                        <br><br>
                        Под привязкой к городу понимается: <br>
                        1) наличие названия города в заголовке группы, описании или аватарке<br>
                        2) наличие региональных телефонных номеров в заголовке группы, описании или аватарке<br>
                        3) прямое указание города в графе местоположение группы<br>
                        <br>
                        Также мы не работаем с группами, которые связаны с продажей алкоголя и табачной
                        продукции.<br>
                        <br>
                        Все заказы, которые, по-нашему мнению, не соответствуют заданным критериям, отменяются,
                        деньги возвращаются на баланс.<br>
                        <br>
                        В случае, если у Вас есть сомнения, относительно того, пройдет ваш объект модерацию или нет,
                        Вам стоит спросить об этом техподдержку сервиса, <a href="">создав
                            тикет</a>.
                    </div>
                    <br><br>
                </td>
            </tr>
        </table>


    </div>


</body>
</html>