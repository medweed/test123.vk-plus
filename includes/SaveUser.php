<?php
session_start();
include("LogRegHelper.php");
include("bd.php");
$obj = new  LogRegHelper($DBH);
if(isset($_POST['g-recaptcha-response'])){
    $obj->checkCaptcha($_POST['g-recaptcha-response']);
}

//проверяем пользовательский ввод, если он верный, то функция возвращает TRUE и мы продолжаем работу
if (isset($_POST['login']) and isset($_POST['password']) and isset($_POST['email'])) {
    if (($obj->checkInputLogPas($_POST['login'], $_POST['password'])) == 'TRUE' &&
        ($obj->checkInputEmail($_POST['email'])) == 'TRUE') {
        $login = $_POST['login'];
        $password = $_POST['password'];
        $email = $_POST['email'];
    }
} else {
    header(header('Location:../index.php'));
}

//проверяем введенный код с картинки
//if (isset($_POST['code'])) {
//  $obj->checkInputCode($_POST['code']);
//}

$login = $obj->treatLogin($login);
$password = $obj->treatPassword($password);

//подключаемся к базе данных
$obj->checkEmail($email);
$obj->checkLogin($login);
$obj->registerUser($login, $password, $email);

