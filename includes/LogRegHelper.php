<?php

//вспомогательный класс для регистрации и входа на сайт
class  LogRegHelper
{
    private $DBH;

    function __construct(PDO $DBH)
    {
        $this->DBH = $DBH;
    }

    function checkCaptcha($captcha)
    {
        if (!$captcha) {
            print ("Заполните капчу");
            exit;
        }
        $secret = "6LdlfiYTAAAAAPRU4f39OnC-4vYQdPF2FlAHYvVs";
        $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $_SERVER["REMOTE_ADDR"]), true);

        if ($response["success"] = false) {
            print ("Ошибка при проверке капчи");
            exit;
        }
    }

//проверяем ввел ли пользователь логин и пароль
    function checkInputLogPas($login, $password)
    {
        if ($login == '' || $password == '') {
            unset($login);
            unset($password);
        }
        if (empty($login) || empty($password)) //если пользователь не ввел логин или пароль, то выдаем ошибку и останавливаем скрипт
        {
            print "Заполните поля: имя и пароль";
            exit;
        }
        return true;
    }

    function checkInputEmail($email)
    {
        //регулярное выражение для проверки введенного email
        $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/';
        if ($email == '') {
            unset($email);
        }
        if (empty($email)) {
            print "Заполните поле email";
            exit;
        } elseif (!preg_match($regex, $email)) {
            print "Вы ввели некоректный Email";
            exit();
        }
        return true;
    }

//обрабатываем данные пользователя
    function treatLogin($login)
    {
        $login = stripslashes($login);
        $login = htmlspecialchars($login);
        //удаляем лишние пробелы
        $login = trim($login);

        return $login;
    }

    function treatPassword($password)
    {
        $password = stripslashes($password);
        $password = htmlspecialchars($password);
        $password = trim($password);
        $password = md5($password);//шифруем пароль
        $password = strrev($password);// для надежности добавим реверс
        $password = $password . "b3p6f";

        return $password;
    }

//проверка существует ли такой email
    function checkEmail($email)
    {
        if ($STH = $this->DBH->query("SELECT id FROM users WHERE email='$email'")) {
            $num = $STH->fetchAll(PDO::FETCH_ASSOC);
            if (count($num) >= 1) {
                print "Пользователь с таким Email уже существует";
                exit();
            }
        }
    }

//проверка существует ли такой логин
    function checkLogin($login)
    {
        if ($STH = $this->DBH->query("SELECT id FROM users WHERE login='$login'")) {
            $num = $STH->fetchAll(PDO::FETCH_ASSOC);
            if (count($num) >= 1) {
                print "Логин уже существует";
                exit();
            }
        }
    }

//регистрируем пользователя в базе
    function registerUser($login, $password, $email)
    {
        $_SESSION['token'] = hash('sha256', $this->generateString(8));
        $_SESSION['series'] = hash('sha256', $this->generateString(8));
        $_SESSION['status'] = 0;
        $activation = md5($email . time());
        $STH = $this->DBH->prepare("INSERT INTO users (login,password,token,series,email,activation) 
            VALUES (?,?,?,?,?,?)");
        $STH->bindParam(1, $login);
        $STH->bindParam(2, $password);
        $STH->bindParam(3, $_SESSION['token']);
        $STH->bindParam(4, $_SESSION['series']);
        $STH->bindParam(5, $email);
        $STH->bindParam(6, $activation);
        $result = $STH->execute();
        if (!$result) {
            print "Ошибка регистрации! Попробуйте еще раз";
            exit();
        } else {
            $to = $email;
            $subject = "Подтверждение электронной почты";
            $body = "Привет <br> </br> Ссылка для активации: <a href=\"www.test123.vk-plus.com/activation.php?activation=" . $activation . "&email=" . $email . "\"> Перейти</a>";
            @mail($to, $subject, $body, "Content-Type: text/html; 
                charset=utf-8", "From: robot@shockstudio.com");

            print "Регистрация прошла успешно! \n На ваш email выслано письмо для подтверждения регистрации.";
            ?>

            <script>
                var delay = 3000;
                setTimeout("document.location.href='../index.php'", delay);
            </script>
            <?php
        }
    }

//проверяем логин и пароль, если верные, то выдаем серию и токен
    function checkLoginAndPassword($login, $password)
    {
        if ($STH = $this->DBH->query("SELECT * FROM users WHERE login='$login' AND password='$password'")) {
            $row = $STH->fetch(PDO::FETCH_ASSOC);
            $_SESSION['id'] = $row['id'];
            if (count($row) == 1) {
                print "Вы ввели неверное имя или пароль";
                exit();
            } else {
                $_SESSION['token'] = hash('sha256', $this->generateString(8));
                $_SESSION['series'] = hash('sha256', $this->generateString(8));
                $STHH = $this->DBH->prepare("UPDATE users SET token=?, series=? WHERE id=?");
                $STHH->bindParam(1, $_SESSION['token']);
                $STHH->bindParam(2, $_SESSION['series']);
                $STHH->bindParam(3, $row['id']);
                $STHH->execute();
                ?>
                <script>
                    document.location.reload();
                </script>
                <?php
            }
        } else {
            print "При входе произошла ошибочка";
        };
    }

//генератор случайной строки
    function generateString($length = 8)
    {
        $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
        $numChars = strlen($chars);
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }
        return $string;
    }


    //проверяем токен и серию,
    //если серия верная и токен верный, то выполняется вход с одновременной сменой токена
    //если серия верная, а токе нет, то у пользователя украли куки и пытались с них зайти, просим его выполнить вход
    //@TODO: Добавить удаление токена и серии из базы, если куки украдены
    //@TODO: Протестировать PDO fetch
    function checkTokenAndSeries($token, $series)
    {
        $STH = $this->DBH->query("SELECT id FROM users WHERE series='$series'");
            $row = $STH->fetch(PDO::FETCH_ASSOC);
        if (count($row) == 0) {
            return false;
        } else {
            if ($STH = $this->DBH->query("SELECT id FROM users WHERE token='$token'")) {
                $row2 = $STH->fetch(PDO::FETCH_ASSOC);
                if (count($row) != 0 and ($row['id'] == $row2['id'])) {
                    $_SESSION['token'] = hash('sha256', $this->generateString(8));
                    $_SESSION['series'] = $series;
                    setcookie("token", $_SESSION['token'], time() + 3600 * 24 * 7);
                    setcookie("series", $_SESSION['series'], time() + 86400 * 30);
                    $STH = $this->DBH->prepare("UPDATE users SET token=? WHERE id=?");
                    $STH->bindParam(1, $_SESSION['token']);
                    $STH->bindParam(2, $row['id']);
                    $STH->execute();
                    return true;
                } else {
                    $err = "err";
                    return $err;
                }//2-else
            }//2-if

        }//1-else
    }//func

    function checkStatus($id)
    {
        $STH = $this->DBH->query("SELECT status FROM users WHERE id='$id'");
        $rows = $STH->fetch(PDO::FETCH_ASSOC);
        return $rows['status'];
    }
}//class