<?php
//выполняем вход
session_start();
include("bd.php");
include("LogRegHelper.php");
$obj = new  LogRegHelper($DBH);
if(isset($_POST['g-recaptcha-response'])){
    $obj->checkCaptcha($_POST['g-recaptcha-response']);
}
//проверяем пользовательский ввод, если он верный, то функция возвращает TRUE и мы продолжаем работу
if (isset($_POST['login']) and isset($_POST['password'])) {
    if ($obj->checkInputLogPas($_POST['login'], $_POST['password']) == 'TRUE') {
        $login = $_POST['login'];
        $password = $_POST['password'];
    }
} else{
    print "Произошла непредвиденная ошибка попробуйте еще раз";

}
$login = $obj->treatLogin($login);
$password = $obj->treatPassword($password);
// подключаемся к базе
$obj->checkLoginAndPassword($login, $password);